<?php

/**
 *  Localization File
 * 
 *  This file  inclueds a lot of massages such as errors, warnings and informations massages for human
 *  
 *  Language: Turkish
 * 
 *  @author Murat Ödünç <murat.asya@gmail.com>
 *  
 */

return array(
    
    'unauthorized'              => 'Yetkisiz',
    'unauthorized_long'         => 'Bu istek için yetki yok.',
    'notfound'                  => 'Bulunamadı',
    'gohome'                    => 'Ana Sayfaya git',
    
    
    
);

