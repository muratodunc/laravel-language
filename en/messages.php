<?php

/**
 *  Localization File
 * 
 *  This file  inclueds a lot of massages such as errors, warnings and informations massages for human
 *  
 *  Language: English
 * 
 *  @author Murat Ödünç <murat.asya@gmail.com>
 *  
 */

return array(
    
    'unauthorized'              => 'Unauthorized',
    'unauthorized_long'         => 'It is not to have authorization for this request',
    'notfound'                  => 'Not Found',
    'gohome'                    => 'Go Home',
    
    
     
    
);

